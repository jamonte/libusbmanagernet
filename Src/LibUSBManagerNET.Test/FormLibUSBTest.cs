﻿/* ==============================================================================	
    LibUSBManagerNET.Test
    Version: 0.1.0b

    José Angel Montelongo Reyes - ja.montelongo[AT]gmail[DOT]com    
    June 2.009	  	  
   ============================================================================== */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LibUSBManagerNET;

namespace LibUSBManagerNET.Test
{
    public enum AppState
    {
        Init,
        DeviceArrived,
        DeviceRemoved
    }

    public partial class FormLibUSBManagerNETTest : Form
    {
        private DeviceDetector deviceDetector;
        private Timer appTimer;
        private AppState appState;
        private string lastDrive;

        #region Private

        private void FillTree(TreeView tree, List<List<string>> list, string title)
        {
            // Insert the root node                        
            TreeNode root = new TreeNode(title + " (" + list.Count.ToString("N0") + ")", 0, 1);
            tree.Nodes.Add(root);

            foreach (List<string> sublist in list)
            {
                // Insert the root node                        
                TreeNode device = new TreeNode(sublist[0].ToString() + " (" + (sublist.Count - 1).ToString("N0") + ")", 0, 1);
                sublist.RemoveAt(0);
                root.Nodes.Add(device);

                foreach (string item in sublist)
                {
                    device.Nodes.Add(new TreeNode(item, 2, 2));
                }
            }
        }

        private void GetDeviceInfo()
        {
            Cursor = Cursors.WaitCursor;

            // Clear all info
            treeViewInfo.Nodes.Clear();

            // Get disk info
            FillTree(treeViewInfo, DeviceUSB.GetInfo(TableDiskInfo.Win32_DiskDrive), "Win32_DiskDrive");
            FillTree(treeViewInfo, DeviceUSB.GetInfo(TableDiskInfo.Win32_PhysicalMedia), "Win32_PhysicalMedia");
            FillTree(treeViewInfo, DeviceUSB.GetInfo(TableDiskInfo.Win32_LogicalDiskToPartition), "Win32_LogicalDiskToPartition");
            FillTree(treeViewInfo, DeviceUSB.GetInfo(TableDiskInfo.Win32_LogicalDisk), "Win32_LogicalDisk");
            FillTree(treeViewInfo, DeviceUSB.GetInfo(TableDiskInfo.Win32_DiskDriveToDiskPartition), "Win32_DiskDriveToDiskPartition");
            FillTree(treeViewInfo, DeviceUSB.GetInfo(TableDiskInfo.Win32_DiskPartition), "Win32_DiskPartition");

            Cursor = Cursors.Default;
        }

        private string FormatTime(DateTime time)
        {
            return time.ToString("HH:mm:ss," + (time.Millisecond / 10).ToString("D2"));
        }

        private void WriteLogEvents(string msg)
        {
            textBoxLog.Text += "[" + FormatTime(DateTime.Now) + "] " + msg + "\r\n";
        }

        private void EnabledButtonsRemove()
        {
            buttonRemove.Enabled = (comboBoxDrive.Items.Count > 0);
        }

        #endregion

        #region Events

        private void EventDeviceArrived(string drive)
        {
            lastDrive = drive;
            appState = AppState.DeviceArrived;
        }

        private void EventDeviceRemoved(string drive)
        {
            lastDrive = drive;
            appState = AppState.DeviceRemoved;
        }

        private void ActionDeviceRemoved()
        {
            WriteLogEvents("Removed (" + lastDrive + ")");

            comboBoxDrive.Items.Remove(lastDrive);

            EnabledButtonsRemove();
            GetDeviceInfo();
            appState = AppState.Init;
        }

        private void AcctionDeviceArrived()
        {
            string id = DeviceUSB.GetIdFromDrive(lastDrive);

            WriteLogEvents("Arrived (" + lastDrive + ") Id: " + id);
            textBoxId.Text = id;

            if (!comboBoxDrive.Items.Contains(lastDrive))
            {
                comboBoxDrive.Items.Add(lastDrive);
                comboBoxDrive.SelectedItem = lastDrive;
            }

            EnabledButtonsRemove();
            GetDeviceInfo();
            appState = AppState.Init;
        }

        #endregion

        public FormLibUSBManagerNETTest()
        {
            InitializeComponent();

            deviceDetector = new DeviceDetector(this);
            lastDrive = "";

            deviceDetector.eventDeviceArrived += EventDeviceArrived;
            deviceDetector.eventDeviceRemoved += EventDeviceRemoved;
        }

        private void FormLibUSBManagerNETTest_Load(object sender, EventArgs e)
        {
            // create the appTimer
            appTimer = new Timer();
            appTimer.Interval = 10;
            appTimer.Tick += new EventHandler(AppTimer);

            appState = AppState.Init;
            appTimer.Start();
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            if (deviceDetector != null)
            {
                deviceDetector.WndProc(ref m);
            }
        }

        private void FormLibUSBManagerNETTest_Shown(object sender, EventArgs e)
        {
            MinimumSize = Size;

            GetDeviceInfo();
        }

        private void linkLabelWeb_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string url = @"http://jamontelongo.info";

            try
            {
                System.Diagnostics.Process.Start(url);
            }
            catch (Exception exc1)
            {
                if (exc1.GetType().ToString() != "System.ComponentModel.Win32Exception")
                {
                    try
                    {
                        System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo("IExplore.exe", url);
                        System.Diagnostics.Process.Start(startInfo);
                        startInfo = null;
                    }
                    catch
                    {

                    }
                }
            }
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            GetDeviceInfo();
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboBoxDrive.Items.Count > 0)
                {
                    string drive = comboBoxDrive.Text;
                    int nextItem = comboBoxDrive.SelectedIndex - 1;

                    if (nextItem < 0)
                    {
                        nextItem = 0;
                    }

                    comboBoxDrive.Items.Remove(drive);

                    if (comboBoxDrive.Items.Count > 0)
                    {
                        comboBoxDrive.SelectedIndex = nextItem;
                    }

                    DeviceUSB.Unplug(drive, true);
                }
            }
            catch
            {
                MessageBox.Show("Error removing the device.\r\nCheck your privileges.", "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AppTimer(object sender, EventArgs eArgs)
        {
            if (sender == appTimer)
            {
                // Update Statistics
                DateTime endTime = DateTime.Now;

                // Update App States
                switch (appState)
                {
                    case AppState.Init:
                        // nothing to do
                        break;

                    case AppState.DeviceArrived:
                        AcctionDeviceArrived();
                        break;

                    case AppState.DeviceRemoved:
                        ActionDeviceRemoved();
                        break;
                }
            }
        }
    }
}
