﻿/* ==============================================================================	
    LibUSBManager.NET
    Version: 0.1.0b

    José Angel Montelongo Reyes - ja.montelongo[AT]gmail[DOT]com    
    June 2.009	  	  
   ============================================================================== */

using System;
using System.Text;
using System.Collections.Generic;
using System.Management;
using System.Security.Cryptography;
using System.Threading;
using System.Runtime.InteropServices;
using LibUSBManagerNET.UsbEject;

namespace LibUSBManagerNET
{
    public enum TableDiskInfo
    {
        Win32_DiskDrive,
        Win32_PhysicalMedia,
        Win32_LogicalDiskToPartition,
        Win32_LogicalDisk,
        Win32_DiskDriveToDiskPartition,
        Win32_DiskPartition
    }

    static public class DeviceUSB
    {
        private class ThreadDevice
        {
            Volume device;
            bool showUI;

            public ThreadDevice(Volume device, bool showUI)
            {
                this.device = device;
                this.showUI = showUI;
            }

            public void Eject()
            {
                device.Eject(showUI);
            }
        }

        static private void AddItem(List<string> list, ManagementObject item, string atribute)
        {
            if (item[atribute] != null)
            {
                list.Add(atribute + ": " + item[atribute].ToString());
            }
            else
            {
                list.Add(atribute + ": [NONE]");
            }
        }

        static private List<List<string>> GetInfoUsingWin32DiskDrive()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive");
            List<List<string>> list = new List<List<string>>();

            foreach (ManagementObject item in searcher.Get())
            {
                List<string> subList = new List<string>();

                #region Adding items...
                AddItem(subList, item, "Availability");
                AddItem(subList, item, "BytesPerSector");
                AddItem(subList, item, "Caption");
                AddItem(subList, item, "CompressionMethod");
                AddItem(subList, item, "ConfigManagerErrorCode");
                AddItem(subList, item, "ConfigManagerUserConfig");
                AddItem(subList, item, "CreationClassName");
                AddItem(subList, item, "DefaultBlockSize");
                AddItem(subList, item, "Description");
                AddItem(subList, item, "DeviceID");
                AddItem(subList, item, "ErrorCleared");
                AddItem(subList, item, "ErrorDescription");
                AddItem(subList, item, "ErrorMethodology");
                //AddItem(subList, item, "FirmwareRevision");
                AddItem(subList, item, "Index");
                AddItem(subList, item, "InstallDate");
                AddItem(subList, item, "InterfaceType");
                AddItem(subList, item, "LastErrorCode");
                AddItem(subList, item, "Manufacturer");
                AddItem(subList, item, "MaxBlockSize");
                AddItem(subList, item, "MaxMediaSize");
                AddItem(subList, item, "MediaLoaded");
                AddItem(subList, item, "MediaType");
                AddItem(subList, item, "MinBlockSize");
                AddItem(subList, item, "Model");
                AddItem(subList, item, "Name");
                AddItem(subList, item, "NeedsCleaning");
                AddItem(subList, item, "NumberOfMediaSupported");
                AddItem(subList, item, "Partitions");
                AddItem(subList, item, "PNPDeviceID");
                AddItem(subList, item, "PowerManagementSupported");
                AddItem(subList, item, "SCSIBus");
                AddItem(subList, item, "SCSILogicalUnit");
                AddItem(subList, item, "SCSIPort");
                AddItem(subList, item, "SCSITargetId");
                AddItem(subList, item, "SectorsPerTrack");
                //AddItem(subList, item, "SerialNumber");
                AddItem(subList, item, "Signature");
                AddItem(subList, item, "Size");
                AddItem(subList, item, "Status");
                AddItem(subList, item, "StatusInfo");
                AddItem(subList, item, "SystemCreationClassName");
                AddItem(subList, item, "SystemName");
                AddItem(subList, item, "TotalCylinders");
                AddItem(subList, item, "TotalHeads");
                AddItem(subList, item, "TotalSectors");
                AddItem(subList, item, "TotalTracks");
                AddItem(subList, item, "TracksPerCylinder");
                #endregion

                subList.Sort();

                // The first node is the id
                subList.Insert(0, item["Caption"].ToString());

                list.Add(subList);
            }

            return list;
        }

        static private List<List<string>> GetInfoUsingWin32PhysicalMedia()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_PhysicalMedia");
            List<List<string>> list = new List<List<string>>();

            foreach (ManagementObject item in searcher.Get())
            {
                List<string> subList = new List<string>();

                #region Adding items...
                AddItem(subList, item, "Caption");
                AddItem(subList, item, "Description");
                AddItem(subList, item, "InstallDate");
                AddItem(subList, item, "Name");
                AddItem(subList, item, "Status");
                AddItem(subList, item, "CreationClassName");
                AddItem(subList, item, "Manufacturer");
                AddItem(subList, item, "Model");
                AddItem(subList, item, "SKU");
                AddItem(subList, item, "SerialNumber");
                AddItem(subList, item, "Tag");
                AddItem(subList, item, "Version");
                AddItem(subList, item, "PartNumber");
                AddItem(subList, item, "OtherIdentifyingInfo");
                AddItem(subList, item, "PoweredOn");
                AddItem(subList, item, "Removable");
                AddItem(subList, item, "Replaceable");
                AddItem(subList, item, "HotSwappable");
                AddItem(subList, item, "Capacity");
                AddItem(subList, item, "MediaType");
                AddItem(subList, item, "MediaDescription");
                AddItem(subList, item, "WriteProtectOn");
                AddItem(subList, item, "CleanerMedia");
                #endregion

                subList.Sort();

                // The first node is the id
                subList.Insert(0, item["Tag"].ToString());

                list.Add(subList);
            }

            return list;
        }

        static private List<List<string>> GetInfoUsingWin32LogicalDiskToPartition()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_LogicalDiskToPartition");
            List<List<string>> list = new List<List<string>>();

            foreach (ManagementObject item in searcher.Get())
            {
                List<string> subList = new List<string>();

                #region Adding items...
                AddItem(subList, item, "Antecedent");
                AddItem(subList, item, "Dependent");
                AddItem(subList, item, "EndingAddress");
                AddItem(subList, item, "StartingAddress");
                #endregion

                subList.Sort();

                // The first node is the id
                subList.Insert(0, item["Dependent"].ToString());

                list.Add(subList);
            }

            return list;
        }

        static private List<List<string>> GetInfoUsingWin32LogicalDisk()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_LogicalDisk");
            List<List<string>> list = new List<List<string>>();

            foreach (ManagementObject item in searcher.Get())
            {
                List<string> subList = new List<string>();

                #region Adding items...
                AddItem(subList, item, "Access");
                AddItem(subList, item, "Availability");
                AddItem(subList, item, "BlockSize");
                AddItem(subList, item, "Caption");
                AddItem(subList, item, "Compressed");
                AddItem(subList, item, "ConfigManagerErrorCode");
                AddItem(subList, item, "ConfigManagerUserConfig");
                AddItem(subList, item, "CreationClassName");
                AddItem(subList, item, "Description");
                AddItem(subList, item, "DeviceID");
                AddItem(subList, item, "DriveType");
                AddItem(subList, item, "ErrorCleared");
                AddItem(subList, item, "ErrorDescription");
                AddItem(subList, item, "ErrorMethodology");
                AddItem(subList, item, "FileSystem");
                AddItem(subList, item, "FreeSpace");
                AddItem(subList, item, "InstallDate");
                AddItem(subList, item, "LastErrorCode");
                AddItem(subList, item, "MaximumComponentLength");
                AddItem(subList, item, "MediaType");
                AddItem(subList, item, "Name");
                AddItem(subList, item, "NumberOfBlocks");
                AddItem(subList, item, "PNPDeviceID");
                AddItem(subList, item, "PowerManagementSupported");
                AddItem(subList, item, "ProviderName");
                AddItem(subList, item, "Purpose");
                AddItem(subList, item, "QuotasDisabled");
                AddItem(subList, item, "QuotasIncomplete");
                AddItem(subList, item, "QuotasRebuilding");
                AddItem(subList, item, "Size");
                AddItem(subList, item, "Status");
                AddItem(subList, item, "StatusInfo");
                AddItem(subList, item, "SupportsDiskQuotas");
                AddItem(subList, item, "SupportsFileBasedCompression");
                AddItem(subList, item, "SystemCreationClassName");
                AddItem(subList, item, "SystemName");
                AddItem(subList, item, "VolumeDirty");
                AddItem(subList, item, "VolumeName");
                AddItem(subList, item, "VolumeSerialNumber");
                #endregion

                subList.Sort();

                // The first node is the id
                subList.Insert(0, item["Caption"].ToString());

                list.Add(subList);
            }

            return list;
        }

        static private List<List<string>> GetInfoUsingWin32DiskDriveToDiskPartition()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDriveToDiskPartition");
            List<List<string>> list = new List<List<string>>();

            foreach (ManagementObject item in searcher.Get())
            {
                List<string> subList = new List<string>();

                AddItem(subList, item, "Antecedent");
                AddItem(subList, item, "Dependent");

                subList.Sort();

                // The first node is the id
                subList.Insert(0, item["Dependent"].ToString());

                list.Add(subList);
            }

            return list;
        }

        static private List<List<string>> GetInfoUsingWin32DiskPartition()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_DiskPartition");
            List<List<string>> list = new List<List<string>>();

            foreach (ManagementObject item in searcher.Get())
            {
                List<string> subList = new List<string>();

                #region Adding items...
                AddItem(subList, item, "Access");
                AddItem(subList, item, "Availability");
                AddItem(subList, item, "BlockSize");
                AddItem(subList, item, "Bootable");
                AddItem(subList, item, "BootPartition");
                AddItem(subList, item, "Caption");
                AddItem(subList, item, "ConfigManagerErrorCode");
                AddItem(subList, item, "ConfigManagerUserConfig");
                AddItem(subList, item, "CreationClassName");
                AddItem(subList, item, "Description");
                AddItem(subList, item, "DeviceID");
                AddItem(subList, item, "DiskIndex");
                AddItem(subList, item, "ErrorCleared");
                AddItem(subList, item, "ErrorDescription");
                AddItem(subList, item, "ErrorMethodology");
                AddItem(subList, item, "HiddenSectors");
                AddItem(subList, item, "Index");
                AddItem(subList, item, "InstallDate");
                AddItem(subList, item, "LastErrorCode");
                AddItem(subList, item, "Name");
                AddItem(subList, item, "NumberOfBlocks");
                AddItem(subList, item, "PNPDeviceID");
                //AddItem(subList, item, "PowerManagementCapabilities[]");
                AddItem(subList, item, "PowerManagementSupported");
                AddItem(subList, item, "PrimaryPartition");
                AddItem(subList, item, "Purpose");
                AddItem(subList, item, "RewritePartition");
                AddItem(subList, item, "Size");
                AddItem(subList, item, "StartingOffset");
                AddItem(subList, item, "Status");
                AddItem(subList, item, "StatusInfo");
                AddItem(subList, item, "SystemCreationClassName");
                AddItem(subList, item, "SystemName");
                AddItem(subList, item, "Type");
                #endregion

                subList.Sort();

                // The first node is the id
                subList.Insert(0, item["Caption"].ToString());

                list.Add(subList);
            }

            return list;
        }

        static private string EncodeSerial(string serial)
        {
            Byte[] originalBytes;
            Byte[] encodedBytes;
            MD5 md5;

            md5 = new MD5CryptoServiceProvider();
            originalBytes = ASCIIEncoding.Default.GetBytes(serial);
            encodedBytes = md5.ComputeHash(originalBytes);

            return BitConverter.ToString(encodedBytes);
        }

        static private string GenerateIdUsingSignatureAndPNP(ManagementObject item)
        {
            return EncodeSerial(
                item["Signature"].ToString().Replace(" ", "") +
                item["PNPDeviceID"].ToString().Replace(" ", ""));
        }

        static public string GetIdFromDrive(string drive)
        {
            string id = "";

            ManagementObjectSearcher searcherLogical = new ManagementObjectSearcher("SELECT * FROM Win32_LogicalDiskToPartition");

            // 1º Find the disk index
            foreach (ManagementObject item in searcherLogical.Get())
            {
                string pattern = ("Win32_LogicalDisk.DeviceID=\"" + drive[0].ToString().ToUpper() + ":\"").ToUpper();
                string dependent = item["Dependent"].ToString().Replace(" ", "").ToUpper();

                // Find the pattern
                if (dependent.LastIndexOf(pattern) >= 0)
                {
                    // Get the drive letter
                    string antecedent = item["Antecedent"].ToString().Replace(" ", "").ToUpper();
                    int indexOfDisk = antecedent.LastIndexOf("DISK#");

                    if (indexOfDisk >= 0)
                    {
                        int driveIndex = Convert.ToInt32(antecedent.Substring(indexOfDisk + 5, 1));

                        ManagementObjectSearcher searcherDisk =
                            new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive WHERE Index = " + driveIndex);

                        if (searcherDisk.Get().Count == 1)
                        {
                            foreach (ManagementObject itemDisk in searcherDisk.Get())
                            {
                                return GenerateIdUsingSignatureAndPNP(itemDisk);
                            }
                        }
                        else
                        {
                            // throw new Exception("DiskIndex not found.");
                        }
                    }
                    else
                    {
                        // throw new Exception("Disk# not found.");
                    }
                }
            }

            return id;
        }

        static public List<List<string>> GetInfo(TableDiskInfo table)
        {
            switch (table)
            {
                case TableDiskInfo.Win32_DiskDrive:
                    return GetInfoUsingWin32DiskDrive();

                case TableDiskInfo.Win32_PhysicalMedia:
                    return GetInfoUsingWin32PhysicalMedia();

                case TableDiskInfo.Win32_LogicalDiskToPartition:
                    return GetInfoUsingWin32LogicalDiskToPartition();

                case TableDiskInfo.Win32_LogicalDisk:
                    return GetInfoUsingWin32LogicalDisk();

                case TableDiskInfo.Win32_DiskDriveToDiskPartition:
                    return GetInfoUsingWin32DiskDriveToDiskPartition();

                case TableDiskInfo.Win32_DiskPartition:
                    return GetInfoUsingWin32DiskPartition();

                // By default, return a empty list
                default:
                    return new List<List<string>>();
            }
        }

        static public void Unplug(string drive, bool showUI)
        {
            VolumeDeviceClass volumeDeviceClass = new VolumeDeviceClass();

            foreach (Volume device in volumeDeviceClass.Devices)
            {
                // Is this volume on USB disks?
                if (!device.IsUsb)
                {
                    continue;
                }

                // Is this volume a logical disk?
                if ((device.LogicalDrive == null) || (device.LogicalDrive.Length == 0))
                {
                    continue;
                }

                // Allow Windows to display any relevant UI
                if (device.LogicalDrive[0] == drive[0])
                {
                    ThreadDevice threadDevice = new ThreadDevice(device, showUI);
                    Thread thread = new Thread(threadDevice.Eject);

                    thread.Start();
                    break;
                }
            }
        }
    }
}
