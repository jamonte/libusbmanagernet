﻿/* ==============================================================================	
    LibUSBManager.NET
    Version: 0.1.0b

    José Angel Montelongo Reyes - ja.montelongo[AT]gmail[DOT]com    
    June 2.009	  	  
   ============================================================================== */

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace LibUSBManagerNET
{
    public delegate void EventDeviceDetector(string drive);

    public class DeviceDetector
    {
        // Events
        public event EventDeviceDetector eventDeviceArrived;
        public event EventDeviceDetector eventDeviceRemoved;

        private IntPtr handle;

        // WIN32 Constants
        private const int WM_DEVICECHANGE = 0x0219;
        private const int DBT_DEVTYP_VOLUME = 2;
        private const int DBT_DEVTYP_HANDLE = 6;
        private const int DBT_DEVICEARRIVAL = 0x8000;
        private const int DBT_DEVICEQUERYREMOVE = 0x8001;
        private const int DBT_DEVICEREMOVECOMPLETE = 0x8004;
        private const int BROADCAST_QUERY_DENY = 0x424D5144;

        // Struct for parameters of the WM_DEVICECHANGE message
        [StructLayout(LayoutKind.Sequential)]
        private struct DEV_BROADCAST_VOLUME
        {
            public int dbcv_devicetype;
            public int dbcv_reserved;
            public int dbcv_size;
            public int dbcv_unitmask;
        }

        private string DriveMaskToLetter(int mask)
        {
            char letter;
            string drives = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            int cnt = 0;
            int pom = mask / 2;

            while (pom != 0)
            {
                pom = pom / 2;
                cnt++;
            }

            if (cnt < drives.Length)
            {
                letter = drives[cnt];
            }
            else
            {
                letter = '?';
            }

            return letter.ToString() + ":\\";
        }

        public DeviceDetector(Control control)
        {
            handle = control.Handle;
        }

        public void WndProc(ref Message message)
        {
            int devType;
            DEV_BROADCAST_VOLUME volume;
            string driver;

            if (message.Msg == WM_DEVICECHANGE)
            {
                switch (message.WParam.ToInt32())
                {
                    case DBT_DEVICEARRIVAL: // New device has just arrived

                        devType = Marshal.ReadInt32(message.LParam, 4);
                        volume = (DEV_BROADCAST_VOLUME)Marshal.PtrToStructure(message.LParam, typeof(DEV_BROADCAST_VOLUME));
                        driver = DriveMaskToLetter(volume.dbcv_unitmask);

                        if (devType == DBT_DEVTYP_VOLUME)
                        {
                            if (eventDeviceArrived != null)
                            {
                                eventDeviceArrived(driver);
                            }
                        }

                        break;

                    case DBT_DEVICEREMOVECOMPLETE: // Device has been removed

                        devType = Marshal.ReadInt32(message.LParam, 4);
                        volume = (DEV_BROADCAST_VOLUME)Marshal.PtrToStructure(message.LParam, typeof(DEV_BROADCAST_VOLUME));
                        driver = DriveMaskToLetter(volume.dbcv_unitmask);

                        if (devType == DBT_DEVTYP_VOLUME)
                        {
                            if (eventDeviceRemoved != null)
                            {
                                eventDeviceRemoved(driver);
                            }
                        }

                        break;
                }
            }
        }
    }
}
