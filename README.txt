﻿  ---------------------
    LibUSBManager.NET
  ---------------------

   - Version: 0.1.0 
   - Last Updated: 19/06/2009   
   - Author: José Ángel Montelongo Reyes
   - Email: ja.montelongo[AT]gmail[DOT]com
   - Web: http://jamontelongo.info

   LibUsbManager.NET is a simple library developed in C# that manages events 
   that occur when you insert or remove a USB storage device. It also has methods 
   to get information from the device or disconnect it safely.

   The main methods are:
   
    - GetInfo(): Gets device info.
    - GetIdFromDrive(): Gets a unique identifier for each device.
    - Unplug(): Disconnect a USB device safely.
    - EventDeviceArrived: Event that occurs when a USB device is conected.
    - EventDeviceRemoved: Event generated when a USB device is disconnected.
